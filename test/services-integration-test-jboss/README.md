Integration Test
================
## Requirements
**NOTE!** The integration tests requires a running MySQL with the database 'goldenzone_it'.
connection-url: jdbc:mysql://localhost:3306/goldenzone_it.
user: test
password: test

## Setup
The integration test will setup a JBoss AS with the test configuration (jboss-config-<project version>-test.zip) and
deploy the app-ear-<project version>.ear application on it.

### DB loader
**Note** In order for it to work you must run mvn install on the tools/db/db-loader project

## Execute
The integration tests will only be executed when the integration-test profile is active.

Run it as follows:

    $ mvn verify -Pint-test

## Ports
By default the JBoss ports are offset by 10000 (e.g the HTTP port will be at 18080), if that is inappropriate change the following properties:

        <servlet.port>18080</servlet.port>
        <management.port>19999</management.port>
        <remoting.transport.port>14447</remoting.transport.port>
        <jrmp.port>11090</jrmp.port>
        <jmx.port>11091</jmx.port>
        <rmi.port>11099</rmi.port>

or set on the command line:

    $ mvn verify -Pint-test -Dservlet.port=28080

